===
vma
===

-------------------------------------
create, extract and inspect VMA files
-------------------------------------

:Author: Wolfgang Bumiller <wry+proxmox@errno.eu>
:Manual section: 1
:Manual group: vma Manual

SYNOPSIS
========

``vma list`` VMAFILE

``vma config`` VMAFILE [[``-c``] CONFIGFILES]...

``vma extract`` VMAFILE DESTDIR

``vma verify`` VMAFILE

``vma create`` VMAFILE [\ *OPTIONS*\ ]\ DISKFILES...

DESCRIPTION
===========

``vma`` is a command line tool to create, extract or inspect virtual machine
archives in VMA format as created when creating a virtual machine backup in the
Proxmox Virtual Environment.

``vma list`` lists the configuration files and disk images of an archive.

``vma config`` extracts one or more configuration files from an archive to
stdout.

``vma extract`` unpacks an archive into a destination folder. Disk images will
be extracted in raw format.

``vma verify`` verifies the integrity of a VMA archive. It will warn if there
are disk images with missing blocks or failing checksums.

``vma create`` creates a VMA archive from existing disk images and
configuration files.

OPTIONS for ``vma create``
==========================

``-c``\ *FILENAME*
    Include a file as a configuration file.

``-v``
    Do regular progress reports during the operation.

``-q``
    Don't report progress. This is the default.

BUGS
====

Please report bugs at https://bugzilla.proxmox.com/
