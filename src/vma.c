#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <fcntl.h>
#include <errno.h>
#include <limits.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <inttypes.h>

#include "glib_wrapper.h"
#include "vma-int.h"

#define USAGE_LIST "list VMAFILE\n" \
"    list the contents of a VMA archive\n"

#define USAGE_EXTRACT "extract VMAFILE DESTDIR\n" \
"    extract a vma archive into a destination directory\n"

#define USAGE_VERIFY "verify VMAFILE\n" \
"    verify the contents of a VMA file\n"

#define USAGE_CREATE "create VMAFILE [OPTIONS] files...\n" \
"    create a VMA archive\n" \
"    options:\n" \
"      -c FILE      include this file as config file\n" \
"                   (can be used multiple times)\n" \

#define USAGE_CONFIG "config VMAFILE [OPTIONS] [files...]\n" \
"    dump config files of a VMA archive\n" \
"    options:\n" \
"      -c CFGFILE   dump this particular config\n" \
"                   (can be used multiple times)\n" \

static _Noreturn void
usage(FILE *out, int exitcode) {
	fprintf(out,
"usage: vma <command> [parameters...]\n"
"commands:\n"
"  " USAGE_LIST
"  " USAGE_CONFIG
"  " USAGE_EXTRACT
"  " USAGE_VERIFY
"  " USAGE_CREATE
	);
	exit(exitcode);
}

static int
cmd_list(int argc, char **argv) {
	if (argc != 2) {
		fprintf(stderr, "vma %s", USAGE_LIST);
		return EXIT_FAILURE;
	}

	if (strcmp(argv[1], "-h") == 0 ||
	    strcmp(argv[1], "-?") == 0 ||
	    strcmp(argv[1], "-help") == 0)
	{
		fprintf(stdout, "vma %s", USAGE_LIST);
		return EXIT_SUCCESS;
	}

	VMAReader *vma = VMAReader_fopen(argv[1]);
	if (!vma) {
		fprintf(stderr, "vma list: open: %s\n", strerror(errno));
		return EXIT_FAILURE;
	}

	size_t nFiles;
	const VMAConfigFile *file = VMAReader_getConfigFiles(vma, &nFiles);
	for (size_t i = 0; i != nFiles; ++i)
		printf("CFG: size: %zd name: %s\n", file[i].size, file[i].name);

	size_t nDevices;
	const VMADeviceInfo *devs = VMAReader_getDevices(vma, &nDevices);
	for (size_t i = 0; i != nDevices; ++i) {
		if (strcmp(devs[i].name, "vmstate") == 0) {
			printf("VMSTATE: dev_id=%zd memory: %zd\n",
			       i+1, devs[i].size);
		} else {
			printf("DEV: dev_id=%zd size: %zd devname: %s\n",
			       i+1, devs[i].size, devs[i].name);
		}
	}

	time_t ct = VMAReader_ctime(vma);
	printf("CTIME: %s", ctime(&ct));

	VMAReader_destroy(vma);
	return 0;
}

static int
dump_cfg_file(const VMAConfigFile *files, size_t nFiles, const char *name)
{
	for (size_t i = 0; i != nFiles; ++i) {
		if (strcmp(files[i].name, name) == 0) {
			fwrite(files[i].data, files[i].size, 1, stdout);
			return 0;
		}
	}
	return -ENOENT;
}

static int
cmd_config(int argc, char **argv) {
	const char *archive_filename = NULL;

	VectorOf(const char*) config_files;

	if (argc < 2) {
		fprintf(stderr, "vma %s", USAGE_CONFIG);
		return EXIT_FAILURE;
	}

	if (argv[1][0] != '-') {
		archive_filename = argv[1];
		--argc;
		++argv;
	}

	Vector_new_type(&config_files, char*, NULL);

	int ret = EXIT_SUCCESS;
	int opt;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunreachable-code-break"
	while ((opt = getopt(argc, argv, "+hc:")) != -1) {
		switch (opt) {
		case 'h':
			fprintf(stdout, "vma %s", USAGE_CONFIG);
			goto out;
		case 'c':
			Vector_push(&config_files, &optarg);
			break;

		default:
			fprintf(stderr, "vma %s", USAGE_CONFIG);
			ret = EXIT_FAILURE;
			goto out;
		}
	}
#pragma clang diagnostic pop

	if (!archive_filename) {
		if (optind >= argc) {
			fprintf(stderr, "vma: missing archive filename\n");
			goto out;
		}
		archive_filename = argv[optind++];
	}

	VMAReader *vma = VMAReader_fopen(archive_filename);
	if (!vma) {
		fprintf(stderr, "vma list: open: %s\n", strerror(errno));
		ret = EXIT_FAILURE;
		goto out;
	}

	size_t nFiles;
	const VMAConfigFile *files = VMAReader_getConfigFiles(vma, &nFiles);

	if (!Vector_length(&config_files) && optind >= argc) {
		// by default we print qemu-server.conf
		int err = dump_cfg_file(files, nFiles, "qemu-server.conf");
		if (err < 0) {
			fprintf(stderr, "vma config: qemu-server.conf: %s\n",
			        strerror(-err));
			ret = EXIT_FAILURE;
		}
		goto out;
	}

	const char **name;
	Vector_foreach(&config_files, name) {
		int err = dump_cfg_file(files, nFiles, *name);
		if (err < 0) {
			fprintf(stderr, "vma config: %s: %s\n",
			        *name, strerror(-err));
			ret = EXIT_FAILURE;
		}
	}

	for (; optind < argc; ++optind) {
		int err = dump_cfg_file(files, nFiles, argv[optind]);
		if (err < 0) {
			fprintf(stderr, "vma config: %s: %s\n",
			        argv[optind], strerror(-err));
			ret = EXIT_FAILURE;
		}
	}

	VMAReader_destroy(vma);

out:
	Vector_destroy(&config_files);
	return ret;
}

static int
WriteFunc(void *ud, const struct iovec *iov, size_t count, off_t offset) {
	int *pi = ud;
	if (!iov) {
		// We assume the file got truncated and we can just skip past holes,
		// also since we use pwritev we don't need to seek at all...
		// For debugging:
		//if (fallocate(*pi, FALLOC_FL_KEEP_SIZE | FALLOC_FL_PUNCH_HOLE,
		//              offset, (off_t)count) != 0)
		//{
		//	return -errno;
		//}
	} else {
		ssize_t written = pwritev(*pi, iov, (int)count, offset);
		if (written < 0)
			return -errno;
		// verify (mostly for debugging, see error comments)
		for (size_t i = 0; i != count; ++i)
			written -= iov[i].iov_len;
		if (written < 0) {
			// write less than we wanted, I've only seen this when
			// parts of the iov can be written before running out
			// of space, so:
			return -ENOSPC;
		}
		if (written > 0) // we wrote more than we should have?
			return -EOVERFLOW;
	}
	return 0;
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpadded"
typedef struct {
	int fd;
	char *path;
	char *realpath;
} ExtractFile;
#pragma clang diagnostic pop

static void
ExtractFile_close(ExtractFile *self) {
	close(self->fd);
	g_free(self->path);
	g_free(self->realpath);
}

static int
extract_vma(const char *filename, const char *destination) {
	VMAReader *vma = VMAReader_fopen(filename);
	if (!vma) {
		int err = errno;
		fprintf(stderr, "vma extract: open: %s\n", strerror(err));
		return -err;
	}

	if (mkdir(destination, 0755) != 0 && errno != EEXIST) {
		fprintf(stderr, "vma extract: mkdir failed: %s\n",
		        strerror(errno));
		return -errno;
	}

	VectorOf(ExtractFile) fds;
	Vector_new_type(&fds, ExtractFile, (Vector_dtor*)&ExtractFile_close);

	int err = -EINVAL;
	size_t nFiles;
	const VMAConfigFile *files = VMAReader_getConfigFiles(vma, &nFiles);
	for (size_t i = 0; i != nFiles; ++i) {
		char *path = g_strdup_printf("%s/%s",
		                             destination, files[i].name);
		if (!path) {
			fprintf(stderr, "error allocating path name\n");
			err = -ENOMEM;
			goto errout;
		}
		GError *gerr = NULL;
		if (!g_file_set_contents(path,
		                         files[i].data, (gssize)files[i].size,
		                         &gerr))
		{
			fprintf(stderr, "error writing to file %s: %s\n",
			        path, gerr->message);
			g_error_free(gerr);
		}
		g_free(path);
	}

	// open all files:
	size_t nDevices;
	const VMADeviceInfo *dev = VMAReader_getDevices(vma, &nDevices);
	for (size_t i = 0; i != nDevices; ++i) {
		char *path = g_strdup_printf("%s/tmp-disk-%s.raw",
		                             destination, dev[i].name);
		char *realpath = g_strdup_printf("%s/disk-%s.raw",
		                                 destination, dev[i].name);
		if (!path || !realpath) {
			fprintf(stderr, "error allocating path name\n");
			err = -ENOMEM;
			goto errout;
		}
		int fd = open(path, O_WRONLY|O_CREAT|O_TRUNC, 0644);
		if (fd < 0) {
			err = -errno;
			fprintf(stderr, "open(%s): %s\n",
			        path, strerror(errno));
			g_free(path);
			g_free(realpath);
			goto errout;
		}
		if (ftruncate(fd, (off_t)dev[i].size) != 0) {
			err = -errno;
			fprintf(stderr, "truncate(%s): %s\n",
			        path, strerror(errno));
			g_free(path);
			g_free(realpath);
			goto errout;
		}

		Vector_push(&fds, &(ExtractFile){ fd, path, realpath });
	}

	for (size_t i = 0; i != Vector_length(&fds); ++i) {
		ExtractFile *file = Vector_at(&fds, i);
		err = VMAReader_registerStream(vma, i, &WriteFunc, &file->fd);
		if (err < 0) {
			fprintf(stderr,
			        "vma extract: failed to register stream: %s\n",
			        strerror(-err));
			goto errout;
		}
	}

	// start reading
	ssize_t r;
	do { r = VMAReader_iterate(vma); } while (r > 0);
	if (r < 0) {
		err = (int)err;
		fprintf(stderr, "read error: %s\n", strerror((int)-r));
		goto errout;
	}
	void *it;
	Vector_foreach(&fds, it) {
		ExtractFile *ef = it;
		if (rename(ef->path, ef->realpath) != 0) {
			fprintf(stderr,
			        "vma extract: failed to rename %s to %s\n",
			        ef->path, ef->realpath);
		}
	}

	err = 0;
errout:
	VMAReader_destroy(vma);
	Vector_destroy(&fds);
	return err;
}

static int
cmd_extract(int argc, char **argv) {
	if (argc != 3) {
		fprintf(stderr, "vma %s", USAGE_EXTRACT);
		return EXIT_FAILURE;
	}

	for (size_t i = 1; i != 3; ++i) {
		if (strcmp(argv[i], "-h") == 0 ||
		    strcmp(argv[i], "-?") == 0 ||
		    strcmp(argv[i], "-help") == 0)
		{
			fprintf(stdout, "vma %s", USAGE_EXTRACT);
			return EXIT_SUCCESS;
		}
	}

	if (extract_vma(argv[1], argv[2]) < 0)
		return EXIT_FAILURE;

	return EXIT_SUCCESS;
}

typedef unsigned long bits_t;
static const size_t bits_per_entry = sizeof(bits_t)*8;
typedef struct {
	bits_t *data;
	size_t devid;
	uint64_t devsize;
	uint64_t nblocks;
	bool *write_error;
} bitmap_t;

static void
bitmap_mark_blocks(bitmap_t *self, off_t byteofs, size_t bytelength)
{
	if (!bytelength)
		return;

	//fprintf(stderr, "%zu : %010lx : %zx\n", self->devid, byteofs, bytelength);

	if ((uint64_t)byteofs + (uint64_t)bytelength > self->devsize) {
		*self->write_error = true;

		uint64_t out = (uint64_t)byteofs + (uint64_t)bytelength
		               - self->devsize;
		fprintf(stderr,
		        "write of size %zu to device %zu"
		        " at offset 0x%" PRIx64
		        " is %" PRIu64
		        " bytes out of bounds (device size is 0x%" PRIx64 ")\n",
		        bytelength,
		        self->devid+1,
		        (uint64_t)byteofs,
		        out,
		        self->devsize);
		return;
	}

	// this could be improved...
	uint64_t from = (uint64_t)byteofs / VMA_BLOCK_SIZE;
	uint64_t to = ((uint64_t)byteofs + (uint64_t)bytelength) / VMA_BLOCK_SIZE;
	for (; from != to; ++from) {
		if (from >= self->nblocks) {
			fprintf(stderr,
			        "device %zu: write to out of bounds block\n",
			        self->devid+1);
			break;
		}
		//fprintf(stderr, "dev %zu block %lu\n", self->devid, from);
		size_t index = from / bits_per_entry;
		size_t bitidx = from % bits_per_entry;
		bits_t bit = (1UL << bitidx);
		if (self->data[index] & bit) {
			fprintf(stderr,
			        "device %zu: duplicate block %" PRIu64 " (%lx to %lx)\n",
			        self->devid+1, from, byteofs, (uint64_t)byteofs+bytelength);
		}
		self->data[index] |= bit;
	}
}

static inline bool
bitmap_is_marked(const bitmap_t *self, size_t block)
{
	size_t index = block / bits_per_entry;
	size_t bitidx = block % bits_per_entry;
	bits_t bit = (1UL << bitidx);
	return !!(self->data[index] & bit);
}

static void
FreeBitmap(void *data) {
	bitmap_t *bitmap = data;
	free(bitmap->data);
}

static int
VerifyFunc(void *ud, const struct iovec *iov, size_t count, off_t offset) {
	bitmap_t *bitmap = ud;
	if (!iov) {
		bitmap_mark_blocks(bitmap, offset, count);
	} else {
		size_t bytelength = 0;
		for (size_t i = 0; i != count; ++i)
			bytelength += iov[i].iov_len;
		bitmap_mark_blocks(bitmap, offset, bytelength);
	}
	return 0;
}

static int
verify_vma(const char *filename) {
	VMAReader *vma = VMAReader_fopen(filename);
	if (!vma) {
		int err = errno;
		fprintf(stderr, "vma verify: open: %s\n", strerror(err));
		return -err;
	}

	Vector bitmaps;
	Vector_new_type(&bitmaps, bitmap_t, &FreeBitmap);

	bool had_error = false;

	size_t nDevices;
	const VMADeviceInfo *dev = VMAReader_getDevices(vma, &nDevices);
	for (size_t i = 0; i != nDevices; ++i) {
		uint64_t nblocks = dev[i].size / VMA_BLOCK_SIZE;
		uint64_t entries = VMA_ALIGN_UP(nblocks, bits_per_entry)
		                   / bits_per_entry;
		bitmap_t bitmap = {
			.data = malloc(entries * sizeof(bits_t)),
			.devid = i,
			.devsize = dev[i].size,
			.nblocks = nblocks,
			.write_error = &had_error
		};
		memset(bitmap.data, 0, entries * sizeof(bits_t));
		uint64_t filled_entries = nblocks / bits_per_entry;
		if (filled_entries != entries) {
			// last entry is only partially filled, initialize the
			// remaining bits to 1 for easier verification below
			uint64_t remain = entries * bits_per_entry - nblocks;
			uint64_t lastbit = bits_per_entry - remain;
			bitmap.data[filled_entries] = ~((1<<(lastbit+1))-1);
		}
		Vector_push(&bitmaps, &bitmap);
	}

	int err;
	for (size_t i = 0; i != nDevices; ++i) {
		err = VMAReader_registerStream(vma, i, &VerifyFunc,
		                               Vector_at(&bitmaps, i));
		if (err < 0) {
			fprintf(stderr,
			        "vma verify: failed to register stream: %s\n",
			        strerror(-err));
			goto errout;
		}
	}

	// start reading
	ssize_t r;
	do { r = VMAReader_iterate(vma); } while (r > 0);
	if (r < 0)
		fprintf(stderr, "read error: %s\n", strerror((int)-r));

	for (size_t i = 0; i != nDevices; ++i) {
		const bitmap_t *bitmap = Vector_at(&bitmaps, i);
		for (size_t c = 0; c != bitmap->nblocks; ++c) {
			if (!bitmap_is_marked(bitmap, c)) {
				fprintf(stderr,
				        "device %zu has missing block: %zu\n",
				        i+1, c);
			}
		}
	}

	err = had_error ? EXIT_FAILURE : EXIT_SUCCESS;

errout:
	VMAReader_destroy(vma);
	Vector_destroy(&bitmaps);
	return err;
}

static int
cmd_verify(int argc, char **argv) {
	if (argc != 2) {
		fprintf(stderr, "vma %s", USAGE_VERIFY);
		return EXIT_FAILURE;
	}

	if (strcmp(argv[1], "-h") == 0 ||
	    strcmp(argv[1], "-?") == 0 ||
	    strcmp(argv[1], "-help") == 0)
	{
		fprintf(stdout, "vma %s", USAGE_VERIFY);
		return EXIT_SUCCESS;
	}

	if (verify_vma(argv[1]) < 0)
		return EXIT_FAILURE;

	return EXIT_SUCCESS;
}

static void
print_progress(VMAWriter *vma, bool final) {
	size_t total = VMAWriter_blocks_written(vma);
	size_t zero = VMAWriter_zero_block_count(vma);
	double sparse = ((double)zero/(double)total)*100.;
	total *= VMA_BLOCK_SIZE;
	zero *= VMA_BLOCK_SIZE;
	if (isatty(STDOUT_FILENO)) {
		printf("\r"
		       "written: %16zu bytes (%-4.1f%% sparse, %zu zero bytes)"
		       "\r", total, sparse, zero);
		if (final)
			printf("\n");
		fflush(stdout);
	} else {
		printf("written: %zu bytes (%.1f%% sparse, %zu zero bytes)\n",
		       total, sparse, zero);
	}
}

static void
report_progress(VMAWriter *vma) {
	static time_t last = 0;

	if (!last) {
		last = time(NULL);
		return;
	}

	time_t now = time(NULL);
	if (now == last)
		return;
	last = now;

	print_progress(vma, false);
}

static bool
dump_file(VMAWriter *vma, size_t devid, int fd, const char *name, bool verbose)
{
	static uint8_t buffer[VMA_CLUSTER_SIZE];
	bool eof = false;
	off_t offset = 0;
	bool canseek = true;
	size_t prevbytes = 0;
	while (!eof) {
		ssize_t got;
		const void *writebuf;

		if (canseek) {
			off_t nextdata = lseek(fd, offset, SEEK_DATA);
			if (nextdata < 0) {
				canseek = false;
				goto read_data;
			}
			if (nextdata > offset) {
				off_t align = VMA_ALIGN_DOWN(nextdata,
				                             VMA_BLOCK_SIZE);
				prevbytes = (size_t)(nextdata - align);
				if (prevbytes)
					memset(buffer, 0, prevbytes);
				nextdata = align;
				got = nextdata - offset;
				writebuf = NULL;
				goto write_blocks;
			}
		}

	read_data:
		writebuf = buffer;
		got = read(fd, buffer+prevbytes, sizeof(buffer)-prevbytes);
		prevbytes = 0;
		if (got < 0) {
			fprintf(stderr, "vma: read: %s\n", strerror(errno));
			return false;
		}
		if (!got)
			break;
		if (got > 0 && (size_t)got < sizeof(buffer)) {
			// short read means this is the end, so pad with zeroes
			// for no useful reason ;-)
			memset(buffer + got, 0, sizeof(buffer)-(size_t)got);
			got = VMA_ALIGN_UP(got, VMA_BLOCK_SIZE);
			eof = true;
		}

	write_blocks: ;
		size_t blocks = (size_t)got/VMA_BLOCK_SIZE;
		ssize_t put = VMAWriter_writeBlocks(vma, devid, writebuf,
		                                    blocks, offset);
		if (put < 0 || (size_t)put < blocks) {
			fprintf(stderr, "vma: short write: %s\n",
			        strerror(errno));
			return false;
		}
		offset += got;
		if (verbose)
			report_progress(vma);
	}
	int err = VMAWriter_finishDevice(vma, devid);
	if (err < 0) {
		fprintf(stderr, "vma: error finalizing device %s: %s\n",
		        name, strerror(-err));
		return false;
	}
	return true;
}

static void
CloseFD(void *pfd) {
	close(*(int*)pfd);
}

static int
cmd_create(int argc, char **argv) {
	const char *archive_filename = NULL;
	bool allow_diskless = false;
	unsigned verbosity = 0;
	VectorOf(const char*) config_files;

	Vector_new_type(&config_files, char*, NULL);

	if (argc < 2) {
		fprintf(stderr, "vma %s", USAGE_CREATE);
		return EXIT_FAILURE;
	}

	if (argv[1][0] != '-') {
		archive_filename = argv[1];
		--argc;
		++argv;
	}

	int opt;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunreachable-code-break"
	while ((opt = getopt(argc, argv, "+hvqc:eE")) != -1) {
		switch (opt) {
		case 'h':
			fprintf(stdout, "vma %s", USAGE_CREATE);
			return EXIT_SUCCESS;
			break;
		case 'c':
			Vector_push(&config_files, &optarg);
			break;

		// 'e' for 'empty'
		case 'e': allow_diskless = true; break;
		case 'E': allow_diskless = false; break;
		case 'v': ++verbosity; break;
		case 'q': verbosity = 0; break;

		default:
			fprintf(stderr, "vma %s", USAGE_CREATE);
			return EXIT_FAILURE;
		}
	}
#pragma clang diagnostic pop

	VMAWriter *vma = NULL;
	void *data = NULL;
	size_t datasize = 0;
	int ret = EXIT_FAILURE;
	VectorOf(ExtractFile) fds;
	int err;

	Vector_new_type(&fds, int, &CloseFD);

	if (!archive_filename) {
		if (optind >= argc) {
			fprintf(stderr, "vma: missing archive filename\n");
			goto out;
		}
		archive_filename = argv[optind++];
	}

	if (optind >= argc && !allow_diskless) {
		fprintf(stderr, "vma: refusing to create empty archive\n");
		goto out;
	}

	vma = VMAWriter_fopen(archive_filename);
	if (!vma) {
		fprintf(stderr, "vma: creating writer: %s\n", strerror(errno));
		goto errout;
	}

	const char **cfgs = Vector_data(&config_files);
	for (size_t i = 0; i != Vector_length(&config_files); ++i) {
		ssize_t size = readfile(cfgs[i], &data, &datasize);
		if (size < 0) {
			fprintf(stderr, "vma: failed to read file %s: %s\n",
			        cfgs[i], strerror((int)-size));
			goto errout;
		}
		err = VMAWriter_addConfigFile(vma, cfgs[i], data, (size_t)size);
		if (err < 0) {
			fprintf(stderr, "vma: failed to add file %s: %s\n",
			        cfgs[i], strerror((int)-size));
			goto errout;
		}
	}

	for (int i = optind; i != argc; ++i) {
		int fd = open(argv[i], O_RDONLY);
		if (fd < 0) {
			fprintf(stderr, "vma: open(%s): %s\n",
			        argv[i], strerror(errno));
			goto errout;
		}
		struct stat stbuf;
		if (fstat(fd, &stbuf) != 0) {
			fprintf(stderr, "vma: stat(%s): %s\n",
			        argv[i], strerror(errno));
			close(fd);
			goto errout;
		}
		Vector_push(&fds, &fd);
		ssize_t id = VMAWriter_addDevice(vma, argv[i],
		                                 (size_t)stbuf.st_size);
		if (id < 0) {
			fprintf(stderr, "vma: failed to add device %s: %s\n",
			        argv[i], strerror((int)-id));
			goto errout;
		}
	}

	for (size_t i = 0; i != Vector_length(&fds); ++i) {
		int fd = *(int*)Vector_at(&fds, i);
		if (!dump_file(vma, i, fd, argv[optind+1], verbosity>0))
			goto errout;
	}

	if (verbosity > 0)
		print_progress(vma, true);

	ret = EXIT_SUCCESS;
out:
	free(data);
	Vector_destroy(&config_files);
	Vector_destroy(&fds);
	if (vma) {
		err = VMAWriter_destroy(vma, true);
		if (err < 0)
			fprintf(stderr, "vma: writer: %s\n", strerror(-err));
	}
	return ret;
errout:
	ret = EXIT_FAILURE;
	goto out;
}

int main(int argc, char **argv) {
	if (argc < 2) {
		fprintf(stderr, "vma: missing command\n");
		usage(stderr, EXIT_FAILURE);
	}

	if (!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help"))
		usage(stdout, EXIT_SUCCESS);

	if (!strcmp(argv[1], "extract"))
		return cmd_extract(argc-1, argv+1);
	else if (!strcmp(argv[1], "verify"))
		return cmd_verify(argc-1, argv+1);
	else if (!strcmp(argv[1], "create"))
		return cmd_create(argc-1, argv+1);
	else if (!strcmp(argv[1], "list"))
		return cmd_list(argc-1, argv+1);
	else if (!strcmp(argv[1], "config"))
		return cmd_config(argc-1, argv+1);

	usage(stderr, EXIT_FAILURE);
}
