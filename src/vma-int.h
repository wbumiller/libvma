#ifndef LIBVMA_VMA_INT_H
#define LIBVMA_VMA_INT_H

#include <stdint.h>
#include <stdbool.h>

#include "../include/vma/vma.h"
#include "utils.h"

#ifndef NDEBUG
# define vma_dprint(FMT) fprintf(stderr, "%s", (FMT))
# define vma_dprintf(FMT, ...) fprintf(stderr, "DEBUG: " FMT, __VA_ARGS__)
#else
# define vma_dprintf(...) do{}while(0)
#endif


#define VMA_WRITE_VERSION 1


#define VMA_MAX_CONFIGS 256
#define VMA_MAX_DEVICES 256


#define VMA_BLOCKS_PER_EXTENT 59
#define VMA_CLUSTER_MASK 0x0FFFF
#define VMA_MAX_EXTENT_SIZE \
	(sizeof(VMAExtentHeader) \
	+ VMA_CLUSTER_SIZE * VMA_BLOCKS_PER_EXTENT)


static const uint8_t vma_magic[4] = { 'V', 'M', 'A', 0 };
static const uint8_t vma_extent_magic[4] = { 'V', 'M', 'A', 'E' };


typedef uint32_t blob_addr_t;


typedef struct {
	blob_addr_t devname_addr;
	uint32_t reserved0;
	uint64_t size;
	uint64_t reserved1;
	uint64_t reserved2;
} VMADeviceInfoHeader;


typedef struct {
	uint8_t magic[4];
	uint32_t version;
	uint8_t uuid[16];
	int64_t ctime;
	uint8_t md5sum[16];

	uint32_t blob_buffer_offset;
	uint32_t blob_buffer_size;
	uint32_t header_size;

	uint8_t reserved0[1984];

	blob_addr_t config_names[VMA_MAX_CONFIGS];
	blob_addr_t config_data[VMA_MAX_CONFIGS];

	uint32_t reserved1;

	VMADeviceInfoHeader dev_info[VMA_MAX_DEVICES];
} VMAHeader;

void VMAHeader_etohost(VMAHeader*);
void VMAHeader_etofile(VMAHeader*);
int  VMAHeader_checksum(VMAHeader*, bool verify);
void VMADeviceInfoHeader_etohost(VMADeviceInfoHeader*);
void VMADeviceInfoHeader_etofile(VMADeviceInfoHeader*);


typedef union {
	struct {
		uint32_t cluster;
		uint8_t  dev_id;
		uint8_t  reserved_b0;
		uint16_t mask;
	};
	uint64_t raw;
} VMABlockInfo;


typedef struct {
	uint8_t magic[4];
	uint16_t reserved0;
	uint16_t block_count;
	uint8_t uuid[16];
	uint8_t md5sum[16];
	VMABlockInfo blockinfo[VMA_BLOCKS_PER_EXTENT];
} VMAExtentHeader;

void VMAExtentHeader_etohost(VMAExtentHeader*);
void VMAExtentHeader_etofile(VMAExtentHeader*);
int  VMAExtentHeader_checksum(VMAExtentHeader*, bool verify);


typedef struct {
	VMAExtentHeader header;
	uint8_t data[VMA_MAX_EXTENT_SIZE - sizeof(VMAExtentHeader)];
} VMAExtentBlock;


/* read/write structs */


#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpadded"
typedef struct {
	void *userdata;
	VMADataCB *func;
	struct iovec current[16];
	size_t iovcnt;
	size_t from;
	size_t to;
	bool zeroes;
} VMAReadStream;
#pragma clang diagnostic pop


typedef struct {
	size_t at;
	size_t size;
	const char *data;
} VMABlob;


typedef struct {
	void                   *fh;
	const VMAFileOps       *fops;
	VMAHeader              *header;
	off_t                   blob_beg;
	off_t                   blob_end;
	VectorOf(off_t)         blobs;
} VMACommon;

void VMACommon_init(VMACommon*, const VMAFileOps*, void*);
void VMACommon_destroy(VMACommon*);

ssize_t VMACommon_getBlob(VMACommon*, size_t address, const void **ptr);
ssize_t VMACommon_getString(VMACommon*, size_t address, const void **ptr);
// In write mode:
off_t VMACommon_addBlob(VMACommon*, const void*, size_t);
off_t VMACommon_addBlobString(VMACommon*, const char*);
int VMACommon_growHeader(VMACommon*, size_t);


struct VMAReader {
	VMACommon               common;
	size_t                  fillsize;
	VectorOf(VMADeviceInfo) devices;
	VectorOf(VMAConfigFile) configs;
	VMAReadStream           streams[VMA_MAX_DEVICES];
	VMAExtentBlock          buffer;
};


void VMADeviceInfo_destroy_owned(VMADeviceInfo*);


#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpadded"
typedef struct {
	VMABlockInfo info;
	bool         closed;
	size_t       blocks; // mask bit index
	size_t       bufpos; // non-zero data index in .buffer
	uint8_t      buffer[VMA_CLUSTER_SIZE];
} VMAWriteStream;


struct VMAWriter {
	VMACommon               common;
	bool                    data_inprogress;
	size_t                  configs;
	VectorOf(VMADeviceInfo) devices;
	VMAWriteStream          streams[VMA_MAX_DEVICES];
	size_t                  blocks_total;
	size_t                  blocks_zero;
	size_t                  extent_block;
	VMAExtentBlock          buffer;
};
#pragma clang diagnostic pop

extern const VMAFileOps VMA_FS_FILEOPS;
int vma_fs_open(const char *filename, int mode, int perms, void **handle);
int vma_fs_from_fd(int fd, void **handle);
//ssize_t vma_pread(const VMAFileOps*, void *fh, void *buf, size_t, off_t);


#endif
