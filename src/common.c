#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "vma-int.h"

int
VMAHeader_checksum(VMAHeader *self, bool verify) {
	size_t header_size = BEtoH(self->header_size);
	uint8_t md5sum[NUMOFARRAY(self->md5sum)];
	if (verify)
		memcpy(md5sum, self->md5sum, sizeof(md5sum));
	memset(self->md5sum, 0, sizeof(self->md5sum));
	int err = md5sum_full(self, header_size, self->md5sum);
	if (err < 0)
		return err;
	if (verify && memcmp(self->md5sum, md5sum, sizeof(md5sum)) != 0)
		return -EINVAL;
	return 0;
}

int
VMAExtentHeader_checksum(VMAExtentHeader *self, bool verify) {
	uint8_t md5sum[NUMOFARRAY(self->md5sum)];
	if (verify)
		memcpy(md5sum, self->md5sum, sizeof(md5sum));
	memset(self->md5sum, 0, sizeof(self->md5sum));
	int err = md5sum_full(self, sizeof(*self), self->md5sum);
	if (err < 0)
		return err;
	if (verify && memcmp(self->md5sum, md5sum, sizeof(md5sum)) != 0)
		return -EINVAL;
	return 0;
}


void
VMACommon_init(VMACommon *self, const VMAFileOps *fops, void *fhandle) {
	Vector_new_type(&self->blobs, VMABlob, NULL);
	self->fops = fops;
	self->fh = fhandle;
}

void
VMACommon_destroy(VMACommon *self) {
	if (self->fops && self->fops->close)
		self->fops->close(self->fh);
	Vector_destroy(&self->blobs);
	free(self->header);
}

static VMABlob*
VMACommon_find_blob(VMACommon *self, size_t address) {
	size_t blobcount = Vector_length(&self->blobs);
	if (!blobcount)
		return NULL;

	VMABlob *blobs = Vector_data(&self->blobs);

	size_t a = 0;
	size_t b = blobcount;
	while (a != b) {
		size_t i = (a+b)/2;
		if (address == blobs[i].at)
			return &blobs[i];
		else if (address < blobs[i].at)
			b = i;
		else
			a = i+1;
	}
	return NULL;
}

ssize_t
VMACommon_getBlob(VMACommon *self, size_t address, const void **ptr) {
	VMABlob *blob = VMACommon_find_blob(self, address);
	if (!blob)
		return -ENOENT;
	*ptr = blob->data;
	return (ssize_t)blob->size;
}

ssize_t
VMACommon_getString(VMACommon *self, size_t address, const void **ptr) {
	VMABlob *blob = VMACommon_find_blob(self, address);
	if (!blob)
		return -ENOENT;

	const char *data = blob->data;
	size_t size = blob->size;
	if (data[size-1])
		return -EINVAL;
	*ptr = data;
	return (ssize_t)size;
}

int
VMACommon_growHeader(VMACommon *self, size_t minsize) {
	VMAHeader *header = self->header;
	size_t prev_size = 0;
	if (header) {
		prev_size = header->header_size;
		if (minsize <= prev_size)
			return 0;
	}

	minsize = VMA_ALIGN_UP(minsize, 512);

	header = realloc(header, minsize);
	if (!header)
		return -ENOMEM;
	self->header = header;
	uint8_t *ptr = (uint8_t*)header;
	memset(ptr + prev_size, 0, minsize-prev_size);
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wshorten-64-to-32"
	header->header_size = minsize;
#pragma clang diagnostic pop
	return 0;
}

off_t
VMACommon_addBlob(VMACommon *self, const void *data, size_t size) {
	if (size > 0xFFFF)
		return -ERANGE;

	size_t newsize = (size_t)self->blob_end + size + 2;
	int err = VMACommon_growHeader(self, newsize);
	if (err < 0)
		return err;

	VMAHeader *header = self->header;

	off_t pos = (off_t)self->blob_end;

	uint8_t *raw = (uint8_t*)header;
	// little endian length
	raw[pos+0] = (size>>0) & 0xFF;
	raw[pos+1] = (size>>8) & 0xFF;
	memcpy(raw + pos + 2, data, size);
	self->blob_end += size + 2;

	return pos - self->blob_beg;
}

off_t
VMACommon_addBlobString(VMACommon *self, const char *str) {
	size_t len = strlen(str);
	return VMACommon_addBlob(self, str, len+1);
}

void
VMADeviceInfo_destroy_owned(VMADeviceInfo *self) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wcast-qual"
	free((void*)self->name);
#pragma clang diagnostic pop
}
