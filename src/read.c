#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>

#include "vma-int.h"

void
VMADeviceInfoHeader_etohost(VMADeviceInfoHeader *h) {
	h->devname_addr = BEtoH(h->devname_addr);
	h->size = BEtoH(h->size);
}

void
VMAHeader_etohost(VMAHeader *h) {
	h->version = BEtoH(h->version);
	h->ctime = BEtoH(h->ctime);
	h->blob_buffer_offset = BEtoH(h->blob_buffer_offset);
	h->blob_buffer_size = BEtoH(h->blob_buffer_size);
	h->header_size = BEtoH(h->header_size);
	for (size_t i = 0; i != NUMOFARRAY(h->config_names); ++i) {
		h->config_data[i] = BEtoH(h->config_data[i]);
		h->config_names[i] = BEtoH(h->config_names[i]);
	}
	for (size_t i = 0; i != NUMOFARRAY(h->dev_info); ++i)
		VMADeviceInfoHeader_etohost(&h->dev_info[i]);
}

void VMAExtentHeader_etohost(VMAExtentHeader *h) {
	h->block_count = BEtoH(h->block_count);
	for (size_t i = 0; i != NUMOFARRAY(h->blockinfo); ++i) {
		uint64_t raw = BEtoH(h->blockinfo[i].raw);
		h->blockinfo[i].cluster = (uint32_t)raw;
		h->blockinfo[i].dev_id = (uint8_t)(raw >> 32);
		h->blockinfo[i].mask = (uint16_t)(raw >> (32 + 16));
	}
}

extern const uuid_t*
VMAReader_uuid(VMAReader *self) {
	return (const uuid_t*)self->common.header->uuid;
}

extern time_t
VMAReader_ctime(VMAReader *self) {
	return self->common.header->ctime;
}

extern ssize_t
VMAReader_getBlob(VMAReader *self, size_t address, const void **ptr) {
	return VMACommon_getBlob(&self->common, address, ptr);
}

extern ssize_t
VMAReader_getString(VMAReader *self, size_t address, const void **ptr) {
	return VMACommon_getString(&self->common, address, ptr);
}

extern const VMADeviceInfo*
VMAReader_getDevices(VMAReader *self, size_t *count) {
	if (count)
		*count = Vector_length(&self->devices);
	return Vector_data(&self->devices);
}

extern const VMAConfigFile*
VMAReader_getConfigFiles(VMAReader *self, size_t *count) {
	if (count)
		*count = Vector_length(&self->configs);
	return Vector_data(&self->configs);
}

static int
VMACommon_gatherBlobs(VMACommon *self) {
	uint8_t *data = ((uint8_t*)self->header) + self->blob_beg;
	size_t datasize = (size_t)(self->blob_end - self->blob_beg);
	size_t at = 1;
	while (at != datasize) {
		uint16_t size = (uint16_t)data[at+0];
		size |= ((uint16_t)data[at+1])<<8;

		if (at + size > datasize) {
			vma_dprintf(
			    "Blob at %zu: size %04x ends at %zu of %zu (%u)\n",
			    at, size, at+size, datasize,
			    self->header->blob_buffer_size);
			return -ERANGE;
		}

		VMABlob blob = {
			.at = at,
			.size = size,
			.data = (const char*)(data+at+2)
		};
		Vector_push(&self->blobs, &blob);

		at += size+2;
	}
	return 0;
}

static int
VMAReader_readDevices(VMAReader *self) {
	VMACommon *com = &self->common;
	for (size_t i = 0; i != NUMOFARRAY(com->header->dev_info); ++i) {
		VMADeviceInfoHeader *hdr = &com->header->dev_info[i];
		size_t addr = hdr->devname_addr;
		if (!addr)
			continue;

		VMADeviceInfo info;
		info.id = i;
		info.size = (size_t)hdr->size;
		ssize_t len = VMACommon_getString(com, addr,
		                                  (const void**)&info.name);
		if (len <= 0) {
			vma_dprintf("bad device at %zi: %s\n",
			            addr, strerror((int)-len));
			continue;
		}
		Vector_push(&self->devices, &info);
	}
	return 0;
}

static int
VMAReader_readConfigs(VMAReader *self) {
	VMACommon *com = &self->common;
	for (size_t i = 0; i != NUMOFARRAY(com->header->config_names); ++i) {
		size_t nameoff = com->header->config_names[i];
		size_t dataoff = com->header->config_data[i];
		if (!nameoff || !dataoff)
			continue;
		const void *name;
		ssize_t namelen = VMACommon_getString(com, nameoff, &name);
		if (namelen <= 0) { // name may not be empty
			vma_dprintf("bad config name at %zi: %s\n",
			            nameoff, strerror((int)-namelen));
			continue;
		}
		const void *data;
		ssize_t datalen = VMACommon_getBlob(com, dataoff, &data);
		if (datalen < 0) { // data may be empty
			vma_dprintf("bad config data at %zi: %s\n",
			            dataoff, strerror((int)-datalen));
			continue;
		}
		VMAConfigFile file;
		file.name = name;
		file.data = data;
		file.size = (size_t)datalen;
		Vector_push(&self->configs, &file);
	}
	return 0;
}

extern VMAReader*
VMAReader_create(const VMAFileOps *fops, void *fh) {
	int err;

	VMAReader *self = malloc(sizeof(*self));
	if (!self)
		return NULL;
	memset(self, 0, sizeof(*self));

	VMACommon *com = &self->common;
	VMACommon_init(com, fops, fh);
	Vector_new_type(&self->devices, VMADeviceInfo, NULL);
	Vector_new_type(&self->configs, VMAConfigFile, NULL);

	com->header = malloc(sizeof(*com->header));
	if (!com->header)
		goto out_errno;
	VMAHeader *header = com->header;

	// Read header and check magic values
	err = -EINVAL;
	ssize_t r = fops->read(fh, header, sizeof(*header));
	if (r < 0) {
		err = (int)r;
		goto errout;
	}
	if (r != sizeof(*header) ||
	    memcmp(vma_magic, &header->magic, sizeof(vma_magic)) != 0)
	{
		goto errout;
	}

	// Allocate requested header size
	size_t header_size = BEtoH(header->header_size);
	if (header_size < sizeof(*header))
		goto errout;

	err = -ENOMEM;
	header = realloc(header, header_size);
	if (!header)
		goto errout;
	com->header = header;

	// Read extra header data
	err = -EINVAL;
	r = fops->read(fh, header+1, header_size - sizeof(*header));
	if (r < 0) {
		err = (int)r;
		goto errout;
	}
	if ((size_t)r != header_size - sizeof(*header))
		goto errout;

	// Verify md5sum
	err = VMAHeader_checksum(header, true);
	if (err < 0)
		goto errout;

	// Endian swap
	VMAHeader_etohost(header);

	if (header->version != VMA_WRITE_VERSION) {
		vma_dprintf("unsupported VMA version: %u\n", header->version);
		err = -ERANGE;
		goto errout;
	}

	// Fill data
	com->blob_beg = header->blob_buffer_offset;
	com->blob_end = header->blob_buffer_offset + header->blob_buffer_size;
	err = -EINVAL;
	if ((size_t)com->blob_beg < sizeof(*header)) {
		vma_dprint("invalid blob section start\n");
		goto errout;
	}
	if ((size_t)com->blob_end > header_size) {
		vma_dprint("invalid blob section end\n");
		goto errout;
	}

	// Gather blob info for verification
	err = VMACommon_gatherBlobs(com);
	if (err < 0)
		goto errout;

	// Read devices and configuration
	err = VMAReader_readDevices(self);
	if (err < 0)
		goto errout;

	// Read configurations
	err = VMAReader_readConfigs(self);
	if (err < 0)
		goto errout;

	return self;

out_errno:
	err = -errno;
errout:
	Vector_destroy(&self->configs);
	Vector_destroy(&self->devices);
	com->fops = NULL; // don't close the file on error
	VMACommon_destroy(com);
	free(self);
	errno = -err;
	return NULL;
}

extern VMAReader*
VMAReader_fopen(const char *filename) {
	int ret;
	void *handle;

	ret = vma_fs_open(filename, O_RDONLY, 0, &handle);
	if (ret < 0) {
		errno = -ret;
		return NULL;
	}

	VMAReader *out = VMAReader_create(&VMA_FS_FILEOPS, handle);
	if (!out) {
		ret = errno;
		VMA_FS_FILEOPS.close(handle);
		errno = ret;
	}
	return out;
}

extern void
VMAReader_destroy(VMAReader *self) {
	VMACommon_destroy(&self->common);
	Vector_destroy(&self->configs);
	Vector_destroy(&self->devices);
	free(self);
}

extern int
VMAReader_registerStream(VMAReader *self,
                         size_t dev,
                         VMADataCB *cb,
                         void *data)
{
	if (dev >= Vector_length(&self->devices))
		return -ENOENT;
	VMADeviceInfo *info = Vector_at(&self->devices, dev);
	dev = info->id;
	self->streams[dev].userdata = data;
	self->streams[dev].func = cb;
	return 0;
}

static inline int
VMAReadStream_commitZeroes(VMAReadStream *stream) {
	if (!stream->zeroes)
		return 0;
	stream->zeroes = false;
	size_t to = stream->to;
	size_t from = stream->from;
	return (int)stream->func(stream->userdata, NULL, to-from, (off_t)from);
}

static inline int
VMAReadStream_commitIOVs(VMAReadStream *stream) {
	size_t cnt = stream->iovcnt;
	if (!cnt)
		return 0;
	stream->iovcnt = 0;
	return (int)stream->func(stream->userdata, stream->current, cnt,
	                         (off_t)stream->from);
}

static inline ssize_t
VMAReader_handleBlock(VMABlockInfo *block,
                      VMAReadStream *stream,
                      VMADeviceInfoHeader *dev,
                      uint8_t *data,
                      uint8_t *dataend,
                      size_t part)
{
	int err;

	uint64_t from = (size_t)block->cluster * VMA_CLUSTER_SIZE
	              + part * VMA_BLOCK_SIZE;
	uint64_t to = from + VMA_BLOCK_SIZE;

	if (to > dev->size)
		to = dev->size;

	if (!(block->mask & (1<<part))) {
		if (from >= dev->size)
			return 0;

		if (!stream->zeroes) {
			err = VMAReadStream_commitIOVs(stream);
			if (err < 0)
				return err;
			stream->from = from;
			stream->to = to;
			stream->zeroes = true;
			return 0;
		} else if (from > stream->to || to < stream->from) {
			err = VMAReadStream_commitZeroes(stream);
			if (err < 0)
				return err;
		}
		if (from < stream->from)
			stream->from = from;
		if (to > stream->to)
			stream->to = to;
		stream->zeroes = true;
		return 0;
	}

	if (from >= dev->size) {
		vma_dprintf("out of bounds cluster (%zu > %zu)\n",
		            from, dev->size);
		return -ERANGE;
	}

	err = VMAReadStream_commitZeroes(stream);
	if (err < 0)
		return err;

	const size_t len = to - from;
	if (data + len > dataend)
		return -ERANGE;

	if (stream->iovcnt && from == stream->to) {
		struct iovec *iov = &stream->current[stream->iovcnt-1];
		// can we extend the previous IOV entry?
		uint8_t *oldfrom = iov->iov_base;
		if ((oldfrom + iov->iov_len) == data) {
			stream->to = to;
			iov->iov_len += len;
			return (ssize_t)len;
		}
		// add another entry
		stream->iovcnt++;
		++iov;
		iov->iov_base = data;
		iov->iov_len = len;
		return (ssize_t)len;
	}

	err = VMAReadStream_commitIOVs(stream);
	if (err < 0)
		return err;

	stream->iovcnt++;
	stream->from = from;
	stream->to = to;
	stream->current[0].iov_base = data;
	stream->current[0].iov_len = len;
	return (ssize_t)len;
}

static inline int
VMAExtentHeader_check(VMAExtentHeader *self, const void *uuid) {
	if (memcmp(self->magic, vma_extent_magic, sizeof(self->magic))) {
		vma_dprint("bad extent header magic\n");
		return -EINVAL;
	}

	if (memcmp(self->uuid, uuid, sizeof(self->uuid))) {
		vma_dprint("bad extent header uuid\n");
		return -EINVAL;
	}

	int err = VMAExtentHeader_checksum(self, true);
	if (err < 0)
		return err;
	memset(self->md5sum, 0, sizeof(self->md5sum));

	VMAExtentHeader_etohost(self);
	return 0;
}

static inline int
VMAReader_do_read(VMAReader *self) {
	VMACommon *com = &self->common;
	VMAExtentBlock *block = &self->buffer;
	VMAExtentHeader *header = &block->header;

	bool header_done = (self->fillsize >= sizeof(*header));

	if (header_done) {
		size_t extent_size = sizeof(*header)
		                     + header->block_count * VMA_BLOCK_SIZE;
		if (self->fillsize >= extent_size)
			return 0;
	}

	uint8_t *rdptr = ((uint8_t*)block) + self->fillsize;
	ssize_t got = com->fops->read(com->fh, rdptr,
	                               sizeof(*block) - self->fillsize);
	// errors and EOF are simply handed down
	if (got < 0 || (got == 0 && !self->fillsize))
		return (int)got;

	self->fillsize += (size_t)got;
	if (self->fillsize < sizeof(*header))
		return -EAGAIN;

	if (header_done)
		return 0;

	return VMAExtentHeader_check(header, VMAReader_uuid(self));
}

extern ssize_t
VMAReader_iterate(VMAReader *self) {
	int err = VMAReader_do_read(self);
	if (err < 0)
		return err;

	if (err == 0 && self->fillsize == 0) // EOF
		return 0;

	VMACommon *com = &self->common;
	VMAExtentBlock * const block = &self->buffer;
	VMAExtentHeader * const header = &block->header;

	size_t extent_size = sizeof(*header)
	                     + header->block_count * VMA_BLOCK_SIZE;

	if (extent_size > sizeof(*block)) {
		vma_dprint("extent exceeds maximum size\n");
		return -ERANGE;
	}

	if (extent_size > self->fillsize)
		return -EAGAIN;

	uint8_t *data = block->data;
	uint8_t *dataend = data + extent_size - sizeof(*header);

	// Call the stream callbacks
	for (size_t bi = 0; bi != NUMOFARRAY(header->blockinfo); ++bi) {
		VMABlockInfo *info = &header->blockinfo[bi];
		size_t devid = info->dev_id;
		if (!devid || !self->streams[devid].func)
			continue;
		if (devid >= NUMOFARRAY(com->header->dev_info) ||
		    !com->header->dev_info[devid].devname_addr)
		{
			return -ERANGE;
		}
		VMADeviceInfoHeader *dev = &com->header->dev_info[devid];
		VMAReadStream *stream = &self->streams[devid];
		for (size_t part = 0; part != 16; ++part) {
			ssize_t r = VMAReader_handleBlock(info,
			                                  stream, dev,
			                                  data, dataend,
			                                  part);
			if (r < 0)
				return (int)r;
			data += r;
		}
	}
	for (size_t i = 0; i != NUMOFARRAY(self->streams); ++i) {
		err = VMAReadStream_commitZeroes(&self->streams[i]);
		if (err < 0)
		    return err;
		err = VMAReadStream_commitIOVs(&self->streams[i]);
		if (err < 0)
		    return err;
	}

	if (extent_size < self->fillsize) {
		self->fillsize -= extent_size;
		memmove(block, dataend, self->fillsize);
		if (self->fillsize >= sizeof(*header)) {
			err = VMAExtentHeader_check(header,
			                            VMAReader_uuid(self));
			if (err < 0)
				return err;
		}
	} else {
		self->fillsize = 0;
	}
	// We just return 1 on success.
	return 1;
}

