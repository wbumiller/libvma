#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>

#include <uuid/uuid.h>

#include "vma-int.h"

static int VMAWriter_flush(VMAWriter*);

void
VMADeviceInfoHeader_etofile(VMADeviceInfoHeader *h) {
	h->devname_addr = HtoBE(h->devname_addr);
	h->size = HtoBE(h->size);
}

void
VMAHeader_etofile(VMAHeader *h) {
	h->version = HtoBE(h->version);
	h->ctime = (int64_t)HtoBE(h->ctime);
	h->blob_buffer_offset = HtoBE(h->blob_buffer_offset);
	h->blob_buffer_size = HtoBE(h->blob_buffer_size);
	h->header_size = HtoBE(h->header_size);
	for (size_t i = 0; i != NUMOFARRAY(h->config_names); ++i) {
		h->config_data[i] = HtoBE(h->config_data[i]);
		h->config_names[i] = HtoBE(h->config_names[i]);
	}
	for (size_t i = 0; i != NUMOFARRAY(h->dev_info); ++i)
		VMADeviceInfoHeader_etofile(&h->dev_info[i]);
}

void VMAExtentHeader_etofile(VMAExtentHeader *h) {
	h->block_count = HtoBE(h->block_count);
	for (size_t i = 0; i != NUMOFARRAY(h->blockinfo); ++i)
		h->blockinfo[i].raw = HtoBE(h->blockinfo[i].raw);
}

static int
VMAWriter_writeHeader(VMAWriter *self) {
	VMACommon *com = &self->common;
	com->header->blob_buffer_offset = sizeof(*com->header);
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wshorten-64-to-32"
	com->header->blob_buffer_size = com->blob_end - com->blob_beg;
#pragma clang diagnostic pop

	struct iovec iov;
	iov.iov_base = com->header;
	iov.iov_len = com->header->header_size;
	VMAHeader_etofile(com->header);
	int err = VMAHeader_checksum(com->header, false);
	if (err < 0)
		return err;
	ssize_t written = com->fops->writev(com->fh, &iov, 1);
	err = -errno;
	VMAHeader_etohost(com->header);
	if (written < 0)
		return err;
	if (written != com->header->header_size)
		return -EIO;
	self->data_inprogress = true;
	return 0;
}

extern int
VMAWriter_destroy(VMAWriter *self, bool discard) {
	int err = VMAWriter_flush(self);
	if (err < 0 && !discard)
		return err;
	Vector_destroy(&self->devices);
	VMACommon_destroy(&self->common);
	free(self);
	return err;
}

extern const uuid_t*
VMAWriter_uuid(VMAWriter *self) {
	return (const uuid_t*)self->common.header->uuid;
}

extern int
VMAWriter_set_uuid(VMAWriter *self, void *data, size_t size) {
	// not allowed to change after beginning extents
	if (self->data_inprogress)
		return -EINPROGRESS;
	if (size == 0)
		size = sizeof(self->common.header->uuid);
	else if (size != sizeof(self->common.header->uuid))
		return -EINVAL;
	if (!data)
		uuid_generate(self->common.header->uuid);
	else
		memcpy(self->common.header->uuid, data, size);
	memcpy(self->buffer.header.uuid, self->common.header->uuid, size);
	return 0;
}

extern off_t
VMAWriter_addBlob(VMAWriter *self, const void *data, size_t size) {
	if (self->data_inprogress)
		return -EINPROGRESS;
	return VMACommon_addBlob(&self->common, data, size);
}

extern off_t
VMAWriter_addString(VMAWriter *self, const char *str) {
	return VMAWriter_addBlob(self, str, strlen(str)+1);
}

extern int
VMAWriter_addConfigFile(VMAWriter *self,
                        const char *name,
                        const void *data,
                        size_t size)
{
	if (self->data_inprogress)
		return -EINPROGRESS;

	VMACommon *com = &self->common;
	if (self->configs == NUMOFARRAY(com->header->config_names))
		return -ENOSPC;

	off_t last_blob_end = com->blob_end;

	off_t namepos = VMAWriter_addString(self, name);
	if (namepos < 0)
		return (int)namepos;
	off_t datapos = VMAWriter_addBlob(self, data, size);
	if (datapos < 0) {
		// we don't need the name...
		com->blob_end = last_blob_end;
		return (int)datapos;
	}
	com->header->config_names[self->configs] = (blob_addr_t)namepos;
	com->header->config_data[self->configs] = (blob_addr_t)datapos;
	return (int)self->configs++;
}

extern ssize_t
VMAWriter_addDevice(VMAWriter *self, const char *name, uint64_t bytesize) {
	if (self->data_inprogress)
		return -EINPROGRESS;

	VMACommon *com = &self->common;
	size_t dev_id = Vector_length(&self->devices);
	size_t dev_slot = dev_id+1;
	if (dev_slot == NUMOFARRAY(com->header->dev_info))
		return -ENOSPC;

	VMADeviceInfo info = {
		.id = dev_slot,
		.size = bytesize
	};
	info.name = strdup(name);
	if (!info.name)
		return -ENOMEM;

	off_t namepos = VMAWriter_addString(self, name);
	if (namepos < 0)
		return namepos;

	com->header->dev_info[dev_slot].devname_addr = (blob_addr_t)namepos;
	com->header->dev_info[dev_slot].size = bytesize;

	Vector_push(&self->devices, &info);
	return (ssize_t)dev_id;
}

extern const VMADeviceInfo*
VMAWriter_getDevices(VMAWriter *self, size_t *count) {
	if (count)
		*count = Vector_length(&self->devices);
	return Vector_data(&self->devices);
}

extern ssize_t
VMAWriter_findDevice(VMAWriter *self, const char *name) {
	const size_t len = Vector_length(&self->devices);
	for (size_t i = 0; i != len; ++i) {
		VMADeviceInfo *info = Vector_at(&self->devices, i);
		if (strcmp(name, info->name) == 0)
			return (ssize_t)i;
	}
	return -ENOENT;
}

extern VMAWriter*
VMAWriter_create(const VMAFileOps *fops, void *fh) {
	VMAWriter *self = malloc(sizeof(*self));
	if (!self)
		return NULL;
	memset(self, 0, sizeof(*self));

	VMACommon *com = &self->common;
	VMACommon_init(com, fops, fh);
	Vector_new_type(&self->devices, VMADeviceInfo,
	                (Vector_dtor*)&VMADeviceInfo_destroy_owned);

	// plus 1 for the invalid (index 0) blob address

	int err = VMACommon_growHeader(com, sizeof(*com->header)+1);
	if (err < 0)
		goto errout;
	if (!com->header) {
		err = -ENOMEM;
		goto errout;
	}

	com->blob_beg = sizeof(*com->header);
	com->blob_end = com->blob_beg + 1;

	VMAHeader *header = com->header;
	memcpy(header->magic, vma_magic, sizeof(vma_magic));
	header->version = VMA_WRITE_VERSION;
	header->blob_buffer_offset = sizeof(*header);
	header->blob_buffer_size = 1;

	memcpy(self->buffer.header.magic,
	       vma_extent_magic,
	       sizeof(vma_extent_magic));

	VMAWriter_set_uuid(self, NULL, 0);

	header->ctime = time(NULL);

	return self;

errout:
	com->fops = NULL; // don't close the file on error
	Vector_destroy(&self->devices);
	VMACommon_destroy(&self->common);
	free(self);
	errno = -err;
	return NULL;
}

extern VMAWriter*
VMAWriter_fopen(const char *filename) {
	int ret;
	void *handle;

	ret = vma_fs_open(filename, O_WRONLY | O_CREAT | O_TRUNC, 0644, &handle);
	if (ret < 0) {
		errno = -ret;
		return NULL;
	}

	VMAWriter *out = VMAWriter_create(&VMA_FS_FILEOPS, handle);
	if (!out) {
		ret = errno;
		VMA_FS_FILEOPS.close(handle);
		errno = ret;
	}
	return out;
}

static int
VMAWriter_commit_buffer(VMAWriter *self, bool final) {
	VMACommon *com = &self->common;
	VMAExtentBlock *data = &self->buffer;
	VMAExtentHeader *header = &data->header;

	if (!self->extent_block)
		return 0;
	if (!final && self->extent_block != NUMOFARRAY(header->blockinfo))
		return 0;

	struct iovec iov = {
		.iov_base = data,
		.iov_len = sizeof(*header) +
		           header->block_count * VMA_BLOCK_SIZE
	};

	// Write
	VMAExtentHeader_etofile(header);
	int err = VMAExtentHeader_checksum(header, false);
	if (err < 0)
		return err;
	ssize_t put = com->fops->writev(com->fh, &iov, 1);
	VMAExtentHeader_etohost(header);
	if (put < 0)
		return (int)put;
	if ((size_t)put != iov.iov_len)
		return -EIO;

	self->extent_block = 0;
	header->block_count = 0;
	memset(header->blockinfo, 0, sizeof(header->blockinfo));
	return 0;
}

static int
VMAWriter_commit_stream(VMAWriter *self, VMAWriteStream *stream) {
	VMAExtentHeader *header = &self->buffer.header;
	int err = VMAWriter_commit_buffer(self, false);
	if (err < 0)
		return err;
	uint8_t *dst = self->buffer.data + header->block_count * VMA_BLOCK_SIZE;
	header->blockinfo[self->extent_block].raw = stream->info.raw;
	header->block_count += stream->bufpos / VMA_BLOCK_SIZE;
	memcpy(dst, stream->buffer, stream->bufpos);
	stream->blocks = 0;
	stream->bufpos = 0;
	stream->info.mask = 0;
	stream->info.cluster++;

	self->extent_block++;
	return VMAWriter_commit_buffer(self, false);
}

extern ssize_t
VMAWriter_writeBlocks(VMAWriter *self,
                      size_t device,
                      const void *srcbufv,
                      size_t block_count,
                      off_t offset)
{
	// Once we start writing data we cannot modify the blob buffer anymore.
	if (!self->data_inprogress) {
		int err = VMAWriter_writeHeader(self);
		if (err < 0) {
			errno = -err;
			return err;
		}
	}

	if (device >= Vector_length(&self->devices))
		return -(errno = ERANGE);
	off_t cluster = offset / VMA_CLUSTER_SIZE;

	VMAWriteStream *stream = &self->streams[device];
	if (stream->closed)
		return -(errno = EBADF);
	if (stream->blocks && stream->info.cluster != cluster)
		return -(errno = EINPROGRESS);

	if (!block_count)
		return (errno = 0);

	// commit here in case someone "retries" a write after a failed commit.
	// this will rethrow the same error, (or maybe even succeed if the
	// previous error was EINTR, otherwise we drop out without botching the
	// state).
	if (stream->blocks == 16) {
		int err = VMAWriter_commit_stream(self, stream);
		if (err < 0) {
			errno = -err;
			return err;
		}
	}

	// sanity check for the block position
	off_t cluster_pos = cluster * VMA_CLUSTER_SIZE;
	size_t blocksize = stream->blocks * VMA_BLOCK_SIZE;
	off_t blkoff = cluster_pos + (off_t)blocksize;
	if (blkoff != offset)
		return -(errno = EINVAL);

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wconversion"
#pragma clang diagnostic ignored "-Wshorten-64-to-32"
	stream->info.cluster = cluster;
	stream->info.dev_id = device+1;
#pragma clang diagnostic pop

	ssize_t blocks_done = 0;
	const uint8_t *srcbuf = srcbufv;
	for (size_t i = 0; i != block_count; ++i) {
		if (srcbuf && !is_zero(srcbuf, VMA_BLOCK_SIZE)) {
			memcpy(stream->buffer + stream->bufpos, srcbuf,
			       VMA_BLOCK_SIZE);
			stream->bufpos += VMA_BLOCK_SIZE;
			stream->info.mask |= (1<<stream->blocks);
		} else {
			self->blocks_zero++;
			stream->info.mask &= ~(1<<stream->blocks);
		}
		if (srcbuf)
			srcbuf += VMA_BLOCK_SIZE;
		self->blocks_total++;
		stream->blocks++;

		++blocks_done;

		if (stream->blocks == 16) {
			// commit also bumps the cluster counter
			int err = VMAWriter_commit_stream(self, stream);
			if (err < 0) {
				errno = -err;
				break;
			}
		}
		// NOTE: we need to make sure we clear errno before returning if no
		// error was encountered.
		errno = 0;
	}

	return blocks_done;
}

extern int
VMAWriter_finishDevice(VMAWriter *self, size_t id) {
	if (id >= Vector_length(&self->devices))
		return -ERANGE;
	VMAWriteStream *stream = &self->streams[id];
	if (stream->closed)
		return 0;
	if (stream->blocks) {
		int err = VMAWriter_commit_stream(self, stream);
		if (err < 0)
			return err;
	}
	stream->closed = true;
	return 0;
}

static int
VMAWriter_flush(VMAWriter *self) {
	int err;
	if (!self->data_inprogress) {
		err = VMAWriter_writeHeader(self);
		if (err < 0)
			return err;
	}
	size_t num_devices = Vector_length(&self->devices);
	for (size_t i = 0; i != num_devices; ++i) {
		if (self->streams[i].closed)
			continue;
		err = VMAWriter_finishDevice(self, i);
		if (err < 0)
			return err;
	}
	if (self->extent_block) {
		err = VMAWriter_commit_buffer(self, true);
		if (err < 0)
			return err;
	}

	return 0;
}

extern size_t
VMAWriter_blocks_written(VMAWriter *self) {
	return self->blocks_total;
}

extern size_t
VMAWriter_zero_block_count(VMAWriter *self) {
	return self->blocks_zero;
}
