#ifndef LIBVMA_VMA_UTILS_H
#define LIBVMA_VMA_UTILS_H

#include <stdint.h>
#include <stdbool.h>
#include <endian.h>


#define NUMOFARRAY(X) (sizeof(X)/sizeof((X)[0]))


#define VMA_ALIGN_DOWN(X, A) ( ((X)/(A))*(A) )
#define VMA_ALIGN_UP(X, A) VMA_ALIGN_DOWN((X)+((A)-1), (A))


int md5sum_full(void *data, size_t len, uint8_t *output);

ssize_t readfile(const char*, void**, size_t*);

bool is_zero(const void*, size_t);


typedef void Vector_dtor(void*);
typedef struct {
	uint8_t *data;
	size_t entry_size;
	size_t entry_align;
	size_t slot_size; // entry size aligned to entry alignment
	size_t capacity;
	size_t count;
	Vector_dtor *dtor;
} Vector;

// This is a purely informational macro and does not actually encode the type
#define \
VectorOf(TYPE) Vector

void Vector_new(Vector*, size_t entry_size, size_t align, Vector_dtor*);
void Vector_destroy(Vector*);
void Vector_push(Vector*, const void*);
void* Vector_last(Vector*);
void Vector_pop(Vector*);

#define \
Vector_new_type(VEC, TYP, DTOR) \
	Vector_new((VEC), sizeof(TYP), _Alignof(TYP), (DTOR))

static inline size_t
Vector_length(const Vector *self) {
	return self->count;
}

static inline void*
Vector_at(Vector *self, size_t index) {
	return self->data + index * self->slot_size;
}

static inline void*
Vector_data(Vector *self) {
	return self->data;
}

static inline void*
Vector_begin(Vector *self) {
	return Vector_at(self, 0);
}

static inline void*
Vector_end(Vector *self) {
	return Vector_at(self, self->count);
}

#define \
Vector_advance(VEC, PTR) \
	(*((uint8_t**)&(PTR)) += (VEC)->slot_size)

#define \
Vector_foreach(VEC, PTR) \
	for ((PTR) = Vector_begin(VEC); \
	     (PTR) != Vector_end(VEC); \
	     Vector_advance((VEC), (PTR)))

// For completeness of the BEToH/HtoBE macros we include 1-byte versions.
// Note that _Generic apparently doesn't expand contained macros, so using
// eg. 'be16toh' directly instead of the helpers below causes compile errors.

static inline uint8_t  u_be8toh (uint8_t  u) { return u; }
static inline uint16_t u_be16toh(uint16_t u) { return be16toh(u); }
static inline uint32_t u_be32toh(uint32_t u) { return be32toh(u); }
static inline uint64_t u_be64toh(uint64_t u) { return be64toh(u); }
static inline int8_t   i_be8toh (int8_t  u)  { return u; }
static inline int16_t  i_be16toh(int16_t u)  { return (int16_t)be16toh(u); }
static inline int32_t  i_be32toh(int32_t u)  { return (int32_t)be32toh(u); }
static inline int64_t  i_be64toh(int64_t u)  { return (int64_t)be64toh(u); }

#define BEtoH(X) _Generic((X), \
	uint8_t:  u_be8toh, \
	uint16_t: u_be16toh, \
	uint32_t: u_be32toh, \
	uint64_t: u_be64toh, \
	int8_t:   i_be8toh, \
	int16_t:  i_be16toh, \
	int32_t:  i_be32toh, \
	int64_t:  i_be64toh \
	)(X)

static inline uint8_t  u_le8toh (uint8_t  u) { return u; }
static inline uint16_t u_le16toh(uint16_t u) { return le16toh(u); }
static inline uint32_t u_le32toh(uint32_t u) { return le32toh(u); }
static inline uint64_t u_le64toh(uint64_t u) { return le64toh(u); }
static inline int8_t   i_le8toh (int8_t  u)  { return u; }
static inline int16_t  i_le16toh(int16_t u)  { return (int16_t)le16toh(u); }
static inline int32_t  i_le32toh(int32_t u)  { return (int32_t)le32toh(u); }
static inline int64_t  i_le64toh(int64_t u)  { return (int64_t)le64toh(u); }

#define LEtoH(X) _Generic((X), \
	uint8_t:  u_le8toh, \
	uint16_t: u_le16toh, \
	uint32_t: u_le32toh, \
	uint64_t: u_le64toh, \
	int8_t:   i_le8toh, \
	int16_t:  i_le16toh, \
	int32_t:  i_le32toh, \
	int64_t:  i_le64toh \
	)(X)

static inline uint8_t  u_htobe8 (uint8_t  u) { return u; }
static inline uint16_t u_htobe16(uint16_t u) { return htobe16(u); }
static inline uint32_t u_htobe32(uint32_t u) { return htobe32(u); }
static inline uint64_t u_htobe64(uint64_t u) { return htobe64(u); }
static inline int8_t   i_htobe8 (int8_t  u)  { return u; }
static inline int16_t  i_htobe16(int16_t u)  { return (int16_t)htobe16(u); }
static inline int32_t  i_htobe32(int32_t u)  { return (int32_t)htobe32(u); }
static inline int64_t  i_htobe64(int64_t u)  { return (int64_t)htobe64(u); }

#define HtoBE(X) _Generic((X), \
	uint8_t:  u_htobe8, \
	uint16_t: u_htobe16, \
	uint32_t: u_htobe32, \
	uint64_t: u_htobe64, \
	int8_t:   i_htobe8, \
	int16_t:  i_htobe16, \
	int32_t:  i_htobe32, \
	int64_t:  i_htobe64 \
	)(X)


static inline uint8_t  u_htole8 (uint8_t  u) { return u; }
static inline uint16_t u_htole16(uint16_t u) { return htole16(u); }
static inline uint32_t u_htole32(uint32_t u) { return htole32(u); }
static inline uint64_t u_htole64(uint64_t u) { return htole64(u); }
static inline int8_t   i_htole8 (int8_t  u)  { return u; }
static inline int16_t  i_htole16(int16_t u)  { return (int16_t)htole16(u); }
static inline int32_t  i_htole32(int32_t u)  { return (int32_t)htole32(u); }
static inline int64_t  i_htole64(int64_t u)  { return (int64_t)htole64(u); }

#define HtoLE(X) _Generic((X), \
	uint8_t:  u_htole8, \
	uint16_t: u_htole16, \
	uint32_t: u_htole32, \
	uint64_t: u_htole64, \
	int8_t:   i_htole8, \
	int16_t:  i_htole16, \
	int32_t:  i_htole32, \
	int64_t:  i_htole64 \
	)(X)


#endif
