#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include "glib_wrapper.h"
#include "utils.h"

int
md5sum_full(void *data, size_t len, uint8_t *output) {
	GChecksum *csum = g_checksum_new(G_CHECKSUM_MD5);
	if (!csum)
		return -ENOMEM;
	g_checksum_update(csum, data, (gssize)len);

	gsize csize = 16;
	g_checksum_get_digest(csum, output, &csize);
	g_checksum_free(csum);
	if (csize != 16)
		return -EINVAL;
	return 0;
}

static int
expandbuffer(void **bufptr, size_t *sizeptr, size_t atleast) {
	size_t size = (*bufptr) ? *sizeptr : 0;
	if (size >= atleast)
		return 0;
	if (!size)
		size = 4096;
	while (size < atleast)
		size *= 2;
	void *data = realloc(*bufptr, size);
	if (!data)
		return -ENOMEM;
	*bufptr = data;
	*sizeptr = size;
	return 0;
}

ssize_t
readfile(const char *path, void **dataptr, size_t *sizeptr) {
	static const size_t blocksz = 4096;

	uint8_t *data = *dataptr;
	size_t size = *sizeptr;
	int fd = open(path, O_RDONLY);
	if (fd < 0)
		return -errno;

	ssize_t result = 0;
	size_t at = 0;
	ssize_t got;
	do {
		result = expandbuffer((void**)&data, &size, at+blocksz);
		if (result < 0)
			break;
		got = read(fd, data+at, blocksz);
		if (got < 0) {
			result = -errno;
			break;
		}
		at += (size_t)got;
		result = (ssize_t)at;
	} while (got);

	*dataptr = data;
	*sizeptr = size;
	close(fd);
	return result;
}

bool is_zero(const void *ptr, size_t size) {
#if 0
	uint8_t *b = ptr;
	for (size_t i = 0; i != size; ++i)
		if (b[i])
			return false;
	return true;
#else
#define my_is_aligned(PTR, TYPE) (!( ((uintptr_t)(PTR)) & (_Alignof(TYPE)-1) ))
#define my_align_down(NUM, TYPE) VMA_ALIGN_DOWN((NUM),_Alignof(TYPE))
	const uint8_t *byteptr = ptr;
	const void *end = byteptr + size;

	while (byteptr != end && !my_is_aligned(byteptr, unsigned long)) {
		if (*byteptr++)
			return false;
		--size;
	}
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wcast-align"
	const unsigned long *longptr = (const unsigned long*)byteptr;
#pragma clang diagnostic pop
	const void *aligned_end = byteptr + my_align_down(size, unsigned long);
	while (longptr != aligned_end) {
		if (*longptr++)
			return false;
	}
	byteptr = (const uint8_t*)longptr;
	while (byteptr != end) {
		if (*byteptr++)
			return false;
	}
	return true;
#undef my_align_down
#undef my_is_aligned
#endif
}

void
Vector_new(Vector *self,
           size_t entry_size,
           size_t entry_align,
           Vector_dtor *dtor)
{
	memset(self, 0, sizeof(*self));
	self->entry_size = entry_size;
	self->entry_align = entry_align;
	self->slot_size = VMA_ALIGN_UP(entry_size, entry_align);
	self->dtor = dtor;
}

void
Vector_destroy(Vector *self) {
	if (self->dtor) {
		void *ptr;
		Vector_foreach(self, ptr)
			self->dtor(ptr);
	}
	free(self->data);
	self->data = NULL;
	self->capacity = 0;
	self->count = 0;
}

static void Vector_realloc(Vector *self, size_t newcap) {
	size_t newsize = newcap * self->slot_size;
	uint8_t *newdata;
	do { newdata = realloc(self->data, newsize); } while (!newdata);
	self->data = newdata;
	self->capacity = newcap;
}

static void Vector_more(Vector *self) {
	size_t newcap = self->capacity * 2;
	if (!newcap)
		newcap = 8;
	Vector_realloc(self, newcap);
}

static void Vector_less(Vector *self) {
	size_t newcap = self->capacity;
	while (self->count < newcap/2)
		newcap /= 2;
	if (newcap == self->capacity)
		return;
	Vector_realloc(self, newcap);
}

void*
Vector_last(Vector *self) {
	return self->data + (self->count-1) * self->slot_size;
}

void
Vector_push(Vector *self, const void *entry) {
	if (self->count == self->capacity)
		Vector_more(self);
	memcpy(Vector_end(self), entry, self->entry_size);
	self->count++;
}

void
Vector_pop(Vector *self) {
	if (!self->count)
		return;
	if (self->dtor)
		self->dtor(Vector_last(self));
	self->count--;
	Vector_less(self);
}
