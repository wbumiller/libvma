#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>

#include "vma-int.h"

static void
vma_fops_fclose(void *handleptr) {
	int *pfd = handleptr;
	close(*pfd);
	free(pfd);
}

static ssize_t
vma_fops_read(void *handleptr, void *buffer, size_t size) {
	int *pfd = handleptr;
	ssize_t bytes = read(*pfd, buffer, size);
	if (bytes < 0)
		return -errno;
	return bytes;
}

static ssize_t
vma_fops_writev(void *handleptr,
                const struct iovec *iovec,
                int iovcnt)
{
	int *pfd = handleptr;
	ssize_t bytes = writev(*pfd, iovec, iovcnt);
	if (bytes < 0)
		return -errno;
	return bytes;
}

extern int
vma_fs_open(const char *filename, int mode, int perms, void **handle) {
	int fd = open(filename, mode, perms);
	if (fd < 0)
		return -errno;

	int *pfd = malloc(sizeof(int));
	if (!pfd) {
		close(fd);
		return -ENOMEM;
	}
	*pfd = fd;
	*handle = pfd;
	return 0;
}

extern int
vma_fs_from_fd(int fd, void **handle) {
	int *pfd = malloc(sizeof(int));
	if (!pfd) {
		close(fd);
		return -ENOMEM;
	}
	*pfd = fd;
	*handle = pfd;
	return 0;
}

const VMAFileOps VMA_FS_FILEOPS = {
	.close = vma_fops_fclose,
	.read = vma_fops_read,
	.writev = vma_fops_writev,
};
