#ifndef LIBVMA_VMA_H
#define LIBVMA_VMA_H

#include <sys/types.h>
#include <sys/uio.h>
#include <uuid/uuid.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * \file vma/vma.h
 *
 * \brief Main header for libvma.
 *
 * Include this file to get access to VMAReader and VMAWriter functionality.
 */

/*! Block size and required write granularity for for VMAWriter_writeBlocks.
 */
#define VMA_BLOCK_SIZE 4096
/*! VMA extents are grouped into clusters of 16 4k blocks. */
#define VMA_CLUSTER_SIZE 0x10000

/*!
 * \brief VM Block device.
 *
 * Name and size of a block device.
 */
typedef struct {
	/*!
	 * Device name. Will be used as file name when extracting the archive.
	 */
	const char *name;
	/*! Device size in bytes. */
	uint64_t size;
	/*! VMA-Internal ID of the device. */
	size_t id;
} VMADeviceInfo;


/*!
 * \brief VM Configuration file.
 *
 * VMA archives can short configuration files.
 * These are usually short plaintext files describing the VM.
 */
typedef struct {
	/*!
	 * Configuraiton file name. Will be used as file name when extracting
	 * the archive.
	 */
	const char *name;
	/*! File contents. */
	const char *data;
	/*! File size in bytes. */
	size_t size;
} VMAConfigFile;


/*!
 * \brief File operation callbacks.
 *
 * Read and write operations performed by VMAReader and VMAWriter instances
 * will use these callbacks to read from or write to VMA files. Since VMA files
 * can contain the data out of order, write operations need to support
 * unordered writes.
 */
typedef struct {
	/*!
	 * \brief Closing callback for file handles.
	 *
	 * If not \c NULL, called when destroying a VMAReader or VMAWriter
	 * instance. This will not be called for a failed instantiation of such
	 * objects.
	 *
	 * \param handle The file handle (user data) passed to the
	 *               VMAReader_create or VMAWriter_create functions.
	 */
	void (*close)(void *handle);

	/*!
	 * \brief Read callback used by VMAReader.
	 *
	 * Called by VMAReader to read from an archive.
	 *
	 * \param handle The file handle (user data) passed to the
	 *               VMAReader_create function.
	 * \param buffer The buffer to fill.
	 * \param size The size of the buffer to fill.
	 * \return The number of bytes written into the buffer, or a negative
	 *         \c errno value on error.
	 * \note Short reads are treated as errors by VMAReader.
	 */
	ssize_t (*read)(void *handle, void *buffer, size_t size);

	/*!
	 * \brief Write callback used by VMAWriter.
	 *
	 * Called by VMAWriter to write to an archive. Offsets and sizes are
	 * aligned to VMA_BLOCK_SIZE.
	 *
	 * \param handle The file handle (user data) passed to the
	 *               VMAReader_create function.
	 * \param iov The I/O vector to write out.
	 * \param iovcnt The number of elements in the \p iov I/O vector.
	 * \return The number of bytes written, or a negative \c errno value on
	 *         error.
	 * \note Short writes are treated as errors by VMAWriter.
	 */
	ssize_t (*writev)(void *handle, const struct iovec*, int cnt);
} VMAFileOps;


/*!
 * \brief VMA reader handle.
 *
 * VMA archives are non seekable streams, so you can either have a VMAReader or
 * a VMAWriter object at a time for one archive.
 *
 * Reading happens by iterating over the VMA stream and extracgin the current
 * data. Both the file offsets into block devices and the block devices
 * themselves can be stored intermingled in an archive.
 *
 * Before staring the iteration you must go through the list of contained block
 * devices and subscribe to their device streams. Iterations done beforehand
 * are not recoverable.
 */
typedef struct VMAReader VMAReader;

/*!
 * \brief Device data callback.
 *
 * This is called while iterating over a VMA stream whenever data is
 * encountered for a device.
 *
 * \param userdata Data pointer provided when registering the device stream
 *                 via VMAReader_stream.
 * \param iov I/O vector pointing to the data or \c NULL for blocks of zeroes.
 * \param count If an \p iov was provided, the number of elements. If \p iov
 *              was \c NULL then this is the number zero bytes to write.
 * \param offset Where to write the data or zero bytes.
 */
typedef int VMADataCB(void *userdata,
                      const struct iovec *iov,
                      size_t count,
                      off_t offset);

/*!
 * \brief Open a VMA file for reading.
 * \param filename Path to the file to read.
 * \return A VMA reader on success, otherwise NULL and \c errno is set.
 */
VMAReader *VMAReader_fopen(const char *filename);

/*!
 * \brief Create a VMAReader instance from file operations and user data.
 *
 * This will perform initial read operations to verify the file. Failure of
 * this function can among other issues be caused by a failing of the passed
 * read operation callbacks or unexpected data.
 *
 * \sa VMAFileOps.
 *
 * \param fops File operation callbacks. \see VMAFileOps.
 * \param fh The user data to pass as first parameter to file operation
 *        callbacks.
 * \return A VMA reader on success, otherwise NULL and \c errno is set.
 */
VMAReader *VMAReader_create(const VMAFileOps *fops, void *fh);

/*!
 * \brief Destroy a VMAReader instance.
 *
 * If the provided file operations contain a \c close callback it will be
 * called on the handle (user data) passed to \p VMAReader_create.
 *
 * \param self The VMAReader instance.
 */
void VMAReader_destroy(VMAReader *self);

/*!
 * \brief Get the VMA's uuid.
 *
 * \param self The VMAReader instance.
 * \return The current uuid.
 */
const uuid_t* VMAReader_uuid(VMAReader *self);

/*!
 * \brief Get the VMA's creation time.
 *
 * \param self The VMAReader instance.
 * \return The creation time.
 */
time_t VMAReader_ctime(VMAReader *self);

/*!
 * \brief Get information about the VMA file's block device.
 *
 * This returns to an array of \p VMADeviceInfo structs.
 *
 * \param self The VMAReader instance.
 * \param count If not \c NULL, the number of entries will be stored there.
 * \return Pointer to the device info array.
 */
const VMADeviceInfo *VMAReader_getDevices(VMAReader *self, size_t *count);

/*!
 * \brief Get a list of configuration file data from the VMA archive.
 *
 * Returns a pointer to an array of \p VMAConfigFile structs. The number of
 * entries is optionally written to \p count.
 * Currently the entire data will be available in RAM after opening a VMA file.
 *
 * \param self The VMAReader instance.
 * \param count If not \c NULL, the number of entries will be stored there.
 * \return Pointer to the configuration file array.
 */
const VMAConfigFile *VMAReader_getConfigFiles(VMAReader *self, size_t *count);

/*!
 * \brief Register a data callbacks for a block device.
 *
 * Register a callback called when reading data for a specific device ID.
 * The device ID can be found in the \p VMADeviceInfo structs retrievable via
 * \p VMAReader_getDevices.
 *
 * Each device can only be associated with a single callback. Each call will
 * silently replace any previously registered callback for the same block
 * device.
 *
 * \param self The VMAReader instance.
 * \param dev The device ID. This must be a valid id.
 * \param cb A pointer to the callback function.
 * \param userdata Handle passed as first parameter to the callback function.
 * \return 0 on success, a negative \c errno value on failure.
 *
 * Error values:
 * ~~~
 * Value  | Meaning
 * ------ | -------
 * ENOENT | The device does not exist in the archive.
 * ~~~
 */
int VMAReader_registerStream(VMAReader *self,
                             size_t dev,
                             VMADataCB *cb,
                             void *userdata);

/*!
 * \brief Iterate over the VMA archive.
 *
 * This will read the next cluster from the archive and unpack it via the
 * provided stream callbacks.
 *
 * \param self The VMAReader instance.
 * \return A positive number on success, zero at the end of the archive, or a
 *         negative \c errno value on error.
 */
ssize_t VMAReader_iterate(VMAReader *self);

/*! \brief Get a raw data blob from the VMA's blob section. */
ssize_t VMAReader_getBlob(VMAReader *self, size_t, const void**);
/*! \brief Get a raw string blob from the VMA's blob section. */
ssize_t VMAReader_getString(VMAReader *self, size_t, const void**);


/*!
 * \brief VMA writer handle.
 *
 * Creating a VMA archive must be done the following sequential steps:
 *
 * First the archive handle must be created.
 *
 * Then block devices must be registered and configuration files added.
 *
 * Afterwards the block device data can be written. Once the data is being
 * written the header cannot be modified anymore, this includes modifications
 * to the device information as well as configuration files.
 */
typedef struct VMAWriter VMAWriter;

/*!
 * \brief Open a VMA file for writing.
 *
 * An existing file will be replaced (truncated).
 *
 * \param filename Path to the file to create.
 * \return A VMA writer on success, otherwise NULL and \c errno is set.
 */
VMAWriter *VMAWriter_fopen(const char *filename);

/*!
 * \brief Create a VMAWriter instance from file operations and a handle.
 *
 * This creates an VMAWriter instance using the specified file operations
 * to write to the archive file.
 *
 * \param fops File operation callbacks. \see VMAFileOps.
 * \param fh The user data to pass as first parameter to file operation
 *           callbacks.
 * \return A VMA writer on success, otherwise NULL and \c errno is set.
 */
VMAWriter *VMAWriter_create(const VMAFileOps *fops, void *fh);

/*!
 * \brief Destroy a VMAWriter instance.
 *
 * If there's still data cached in the internal buffers from calls to
 * \p VMAWriter_writeBlocks the data will be flushed first by writing it via
 * the write file operation callback, therefore this call can fail.
 *
 * If this fails the VMAWriter will \b not be destroyed unless the \p discard
 * parameter was set. If \p discard is \c true and this call fails, the
 * resulting VMA archive file will be truncated or broken.
 *
 * \param self The VMAWriter instance.
 * \param discard Force the destruction even if finalization of the data fails.
 * \return 0 on success, a negative \c errno value on error.
 */
int VMAWriter_destroy(VMAWriter *self, bool discard);

/*!
 * \brief Set or randomize the VMA's uuid.
 *
 * VMA Archives contain a UUID. This can be used before writing block device
 * data in order to change this UUID. Note that by default a newly created
 * VMAWriter instance will already generate a random UUID, so this function
 * should not be required.
 *
 * \param self The VMAWriter instance.
 * \param uuid The uuid or \c NULL to create a new random one.
 * \param size This must be 16.
 * \return 0 on success, a negative \c errno value on error.
 * \note This cannot be done after the device data stream has started.
 *
 * Error values:
 * ~~~
 * Value       | Meaning
 * ----------- | -------
 * EINPROGRESS | The data stream has already started. Cannot change the UUID.
 * EINVAL      | A wrong uuid size was passed.
 * ~~~
 */
int VMAWriter_set_uuid(VMAWriter *self, void *uuid, size_t size);

/*!
 * \brief Get the VMA's uuid.
 *
 * \param self The VMAWriter instance.
 * \return The current uuid.
 */
const uuid_t* VMAWriter_uuid(VMAWriter *self);

/*!
 * \brief Add a C string blob to the archive.
 *
 * This is mostly used internally to store device names and configuration
 * failes.
 *
 * \param self The VMAWriter instance.
 * \param text The text to store.
 * \return The resulting offset in the blob section or a negative \c errno
 *         value on error.
 * \note This cannot be done after the device data stream has started.
 */
off_t VMAWriter_addString(VMAWriter *self, const char *text);

/*!
 * \brief Add a data blob to the archive.
 *
 * This is mostly used internally to store device names and configuration
 * failes.
 *
 * \param self The VMAWriter instance.
 * \param data The data to store.
 * \param size The size of \p data .
 * \return The resulting offset in the blob section or a negative \c errno
 *         value on error.
 * \note This cannot be done after the device data stream has started.
 */
off_t VMAWriter_addBlob(VMAWriter *self, const void *data, size_t size);

/*!
 * \brief Add a configuration file entry to the archive.
 *
 * This adds an entry to the configuration file list which will show up in a
 * VMAReader's list retrieved via \p VMAReader_getConfigFiles.
 *
 * \param self The VMAWriter instance.
 * \param name The desired file name.
 * \param data The file's content.
 * \param size The file size.
 * \return 0 on success, a negative \c errno value on error.
 * \note This cannot be done after the device data stream has started.
 */
int VMAWriter_addConfigFile(VMAWriter *self,
                            const char *name,
                            const void *data,
                            size_t size);

/*!
 * \brief Add a block device entry to the archive.
 *
 * This creates a block device entry in the archive. Each block device for
 * which data will later be written via \p VMAWriter_writeBlocks must be
 * registered this way. \p VMAWriter_writeBlocks will refuse to exceed the
 * file size specified here.
 *
 * \param self The VMAWriter instance.
 * \param name The device's name.
 * \param bytesize The device's size in bytes.
 * \return A device index (zero or positive) on success, or a negative \c errno
 *         value on error.
 * \note This cannot be done after the device data stream has started.
 */
ssize_t VMAWriter_addDevice(VMAWriter *self,
                            const char *name,
                            uint64_t bytesize);

/*!
 * \brief Get information about the VMA file's block device.
 *
 * This returns to an array of \p VMADeviceInfo structs.
 *
 * \param self The VMAWriter instance.
 * \param count If not \c NULL, the number of entries will be stored there.
 * \return Pointer to the device info array.
 */
const VMADeviceInfo *VMAWriter_getDevices(VMAWriter *self, size_t *count);

/*!
 * \brief Get a device index by name.
 *
 * This returns the value returned by \c VMAWriter_addDevice if the device
 * has previously been added successfully, \c -ENOENT otherwise.
 *
 * \param self The VMAWriter instance.
 * \param name The device's name.
 */
ssize_t VMAWriter_findDevice(VMAWriter *self, const char *name);

/*!
 * \brief Write block device data.
 *
 * Block device data can only be written in full blocks of size VMA_BLOCK_SIZE,
 * to block aligned offsets, otherwise this function will return an error.
 *
 * Whenever enough blocks to fill an extent have been written the extent will
 * be flushed out to disk via the write callbacks. If this fails the function
 * will not return an error, but instead return how many blocks have
 * successfully been buffered, however, \c errno will be set to the received
 * error. Subsequent calls to this function will try to flush them out again
 * and propagate any errors encountered via both the return value and \c errno.
 *
 * Note that it is an error to call this function on a device stream closed via
 * \p VMAWriter_finishDevice. This will result in an \c EBADF error code
 * (written to \c errno and as negative return value).
 *
 * \param self The VMAWriter instance.
 * \param device The block device the data is meant for.
 * \param buf The data pointer.
 * \param block_count The number of VMA_BLOCK_SIZE blocks in \p buf.
 * \param offset The data's target position in the block device.
 * \return The number of successfully accepted (written or buffered) blocks.
 * \note \c errno should be checked even after apparently successful calls.
 * \note It is possible that all blocks got either written or buffered but the
 *       final write callback failed. In this case the return value will be
 *       equal to \c block_count, suggesting success, but \c errno will be set
 *       to an error value.
 */
ssize_t VMAWriter_writeBlocks(VMAWriter *self,
                              size_t device,
                              const void *buf,
                              size_t block_count,
                              off_t offset);

/*!
 * \brief Close a block device stream.
 *
 * This is a convenience method to mark a block device as done. Afterwards the
 * block device id must not be passed to \p VMAWriter_writeBlocks anymore.
 * Note that this may also flush remaining data into the current extent
 * buffer, so it may make a difference in where the final bytes of a device
 * are stored within the archive.
 *
 * \param self The VMAWriter instance.
 * \param id The block device ID.
 * \return 0 on success, or a negative \c errno value on error.
 * \note This can fail if flushing out the device data results in an extent to
 *       be finished while the write callback fails.
 */
int VMAWriter_finishDevice(VMAWriter *self, size_t id);

/*!
 * \brief Get the number of written 4k blocks.
 * \param self The VMAWriter instance.
 * \return Number of blocks of VMA_BLOCK_SIZE size contained in the archive.
 */
size_t VMAWriter_blocks_written(VMAWriter *self);

/*!
 * \brief Get the number of encountered zero-blocks.
 * \param self The VMAWriter instance.
 * \return Number of blocks consisting solely of zeros in the device streams.
 */
size_t VMAWriter_zero_block_count(VMAWriter *self);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
