include config.mak

doc_SUBDIRS-$(CONFIG_DOC) := doc

SUBDIRS := src include $(doc_SUBDIRS-y)

.PHONY: all
all: $(SUBDIRS) | config.mak

config.mak: configure
	./configure $(CONFIGURE_OPTS)

.PHONY: $(SUBDIRS)
$(SUBDIRS):
	$(MAKE) -C $@

.PHONY: install
install: install-pc
	for i in $(SUBDIRS); do $(MAKE) -C $$i install; done

.PHONY: uninstall
uninstall: uninstall-pc
	for i in $(SUBDIRS); do $(MAKE) -C $$i uninstall; done

.PHONY: clean
clean:
	for i in $(SUBDIRS); do $(MAKE) -C $$i clean; done
	rm -f vma.pc

.PHONY: install-pc
install-pc: vma.pc
	install -dm755 $(DESTDIR)$(PKGCONFIGDIR)
	install -m644 vma.pc $(DESTDIR)$(PKGCONFIGDIR)/vma.pc

.PHONY: uninstall-pc
uninstall-pc:
	rm -f $(DESTDIR)$(PKGCONFIGDIR)/vma.pc
	rmdir $(DESTDIR)$(PKGCONFIGDIR) || true

vma.pc: vma.pc.in
	sed -e 's|@PREFIX@|$(PREFIX)|g' \
	    -e 's|@BINDIR@|$(BINDIR)|g' \
	    -e 's|@LIBDIR@|$(LIBDIR)|g' \
	    -e 's|@INCLUDEDIR@|$(INCLUDEDIR)|g' \
	    -e 's|@PACKAGE_VERSION@|$(PACKAGE_VERSION)|g' \
	    $< > $@
