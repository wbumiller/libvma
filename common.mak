# This is NOT the package/library version info, this is the API version info!
# current:revision:age, see libtool's versioning system
VERSION_INFO = -version-info 0:1:0

# This is the version as reported by pkg-config
PACKAGE_VERSION = 0.1.0

CPPFLAGS += $(LTFLAGS)
CPPFLAGS += -D_GNU_SOURCE -D_FILE_OFFSET_BITS=64

CFLAGS += -std=c11

CPPFLAGS += -Wall -Werror -Wno-unknown-pragmas

ifeq ($(USING_CLANG), 1)
# strdup falls under -Wdisabled-macro-expansion due to its recursive definition
CPPFLAGS += -Weverything \
            -Wno-c++98-compat \
            -Wno-disabled-macro-expansion
endif

CPPFLAGS += $(GLIB2_CFLAGS)
CPPFLAGS += $(UUID_CFLAGS)

.SUFFIXES: .lo
.c.lo:
	$(LIBTOOL) --mode=compile --tag=CC $(CC) $(CPPFLAGS) $(CFLAGS) -c -o $@ $< -MMD -MT $@ -MF $*.d

.c.o:
	$(CC) $(CPPFLAGS) $(CFLAGS) -c -o $@ $< -MMD -MT $@ -MF $*.d
